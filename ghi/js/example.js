// app.js
const clueDiv = document.querySelector('#clue');
console.log(clueDiv);

const url = 'https://jservice.xyz/api/random-clue';
const response = await fetch(url);

function createClueHtml(category, question, answer) {
  return `
    <div>
      <h2>${category}</h2>
      <p><b>Question</b>: ${question}</p>
      <p><b>Answer</b>: ${answer}</p>
    </div>
  `;
}

if (response.ok) {
  console.log(response);

  const data = await response.json();
  console.log(data);

  const category = data.category.title;
  const question = data.question;
  const answer = data.answer;
  const html = createClueHtml(category, question, answer);
  console.log(html);

  clueDiv.innerHTML = html;
} else {
  console.error('Got an error in the response.')
}

const categoriesUrl = 'https://jservice.xyz/api/categories/'
const categoriesResponse = await fetch(categoriesUrl);

if (categoriesResponse.ok) {
  const data = await categoriesResponse.json();

  const selectTag = document.getElementById('categoryId');

  for (let category of data.categories.slice(0, 100)) {
    const option = document.createElement('option');
    option.value = category.id;
    option.innerHTML = category.title;
    selectTag.appendChild(option)
  }

  const form = document.getElementById('create-clue-form');
  form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(form);
    const dataObj = Object.fromEntries(formData);

    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(dataObj),
      headers: {
        'Content-Type': 'application/json'
      },
    }

    try {
      const newClueUrl = 'https://jservice.xyz/api/clues/';
      const newClueResponse = await fetch(newClueUrl, fetchOptions);

      if (newClueResponse.ok) {
        const newClue = await newClueResponse.json();

        const category = newClue.category.title;
        const question = newClue.question;
        const answer = newClue.answer;
        const html = createClueHtml(category, question, answer);
        clueDiv.innerHTML = html;
      }
    } catch (e) {
        console.log(e);
    }
  })
}
